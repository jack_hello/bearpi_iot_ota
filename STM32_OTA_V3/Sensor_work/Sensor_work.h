#ifndef __SENSOR_WORK_H_
#define __SENSOR_WORK_H_

#include "main.h"
#include "usart.h"
#include "stdio.h"
#include "string.h"
#include <ctype.h>
#include <stdlib.h>
#include "E53_IA1.h"
#include "gpio.h"
#include "cmsis_os.h"
#include "task.h"
#include "cJSON.h"
#include "EC600S.h"

//Temperature   Humidity    Light
typedef struct
{
    float    Temperature;  //设备温度  
    float    Humidity;     //设备湿度
	  uint16_t    Light;     //光照强度 
} Sensor_type;
extern Sensor_type E53_Sensor_type;  

void Sensor_Task(void);

#endif



